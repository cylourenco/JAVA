package src;

public class Main {

    public static void main(String[] args) 
    {
        Personne p1 = new Personne("Louane", 20);

        p1.afficherDetails();

        p1.setAge(21);
        System.out.println("Son anniversaire vient de passer !"); 
        p1.afficherDetails();

        Personne p2 = new Personne("Reynalde", 20);
        
        Etudiant e1 = new Etudiant("Antoine", 21, 0);

        p1.afficherDetails();
        p2.afficherDetails();
        e1.afficherDetails();
    
        System.out.println("Nombre total de personne: " + Personne.getNombre());
    
        Personne p3 = new Personne("Cynthia", 121);
        p3.afficherDetails();

        try {
            Personne p4 = new Personne("Cynthia", 121);
            p4.afficherDetails();
        } catch (IllegalArgumentException e) {
            System.out.println("Argument illegal");
        }

        



        
    }

}


// compilation : javac -d bin src/Main.java                                     
// execution : java -cp bin src/Main
