package src;
import java.util.Scanner; 

public class Main {

    public static void main(String[] args) {
        CompteBancaire cb = new CompteBancaire();
        System.out.println(cb.toString());

        Scanner saisieUtilisateur = new Scanner(System.in); 
        System.out.println("Veuillez saisir votre solde (double) :");
        Double nv = saisieUtilisateur.nextDouble(); 

        cb.entree(nv);
        System.out.println(cb.toString());

        try {
            cb.retrait(800);
        } catch (IllegalArgumentException e) {
            System.out.println("Argument illegal");
        } catch (AutorisationException e) {
            System.out.println("Retrait trop important pour le decouvert");
        }
    }

}

// compilation : javac -d bin src/Main.java
// execution : java -cp bin src/Main