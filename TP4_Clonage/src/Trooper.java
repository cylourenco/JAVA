package src;

public class Trooper implements Cloneable {

    private static int compteur = 0;

    public int      id;
    public String   nom;
    public int      niv0Sante;
    public Arme     arme;
    //public Ceinture ceinture;
    public Grade    grade;

    public Trooper(String s, Arme a, Grade g) {
        this.id = this.compteur;
        setNom(s);
        setNiv0Sante();
        this.arme = a;
        this.grade = g;
        compteur++;
    }

    @Override
    public String toString() {
        return "Trooper " + id + 
        " [nom = " + nom + 
        ", niv0Sante = " + niv0Sante + 
        ", arme = " + arme.getNumSerie() + 
        ", grade = " + grade + "]";
            
    }

    public String getNom() {
        return nom;
    }

    public int getNiv0Sante() {
        return niv0Sante;
    }

    public Arme getArme()
    {
        return this.arme;
    }

    public void setNom(String s)
    {
        if (s != null) 
            this.nom = s;
        else
            this.nom = "Clone de Jango Fett #" + this.id;
    }

    public void setNiv0Sante() 
    {
        this.niv0Sante = (int)(Math.random() * 51) + 50;
    }

    public void setNiv0Sante2(int s)
    {
        this.niv0Sante = s;
    }

    @Override
    public Object clone() {
        Trooper object = null;
        try {
            object = (Trooper) (super.clone());
            object.id = compteur++;
        } catch(CloneNotSupportedException cnse) {
            cnse.printStackTrace(System.err);
        }
        
        return object;
    }
}
