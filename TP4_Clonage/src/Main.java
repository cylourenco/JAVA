package src;

public class Main {

    public static void main(String[] args) 
    {
        Arme arme0 = new Fusil(123);
        Trooper trooper0 = new Trooper("Cody", arme0, Grade.COMMANDEUR);

        System.out.println(trooper0.toString());

        Arme arme1 = new Fusil(143);
        Trooper trooper1 = new Trooper(null, arme1, Grade.SERGENT);
    
        System.out.println();
        System.out.println(trooper1.toString());

        Trooper trooper2 = (Trooper)trooper0.clone();

        System.out.println();
        System.out.println(trooper2.toString());

        trooper2.setNom("Mort");
        trooper2.getArme().setNumSerie(1);
        trooper2.setNiv0Sante2(-49); 

        System.out.println();
        System.out.println(trooper0.toString());
        System.out.println(trooper2.toString());
        System.out.println("On voulait modifier seulement l'arme de trooper2 mais l'arme de trooper0 change aussi");

        Trooper2 trooper3 = (Trooper2)trooper1.clone();

        System.out.println();
        System.out.println(trooper1.toString());
    
    }

}


