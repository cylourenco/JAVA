package src;

public class Tableau {
    private Object[] tableau;
    private int capacite;
    private int taille;

    public Tableau()
    {
        capacite = 1;
        tableau = new Object[capacite];
        taille = 0;
    }

    public int getTaille()
    {
        return this.taille;
    }

    public int getCapacite()
    {
        return this.capacite;
    }

    private void agrandir()
    {
        if(this.capacite * 2 >= Integer.MAX_VALUE)
        {
            throw new OutOfMemoryError("La capacité maximale du tableau est atteinte.");
        }

        capacite *= 2;
        Object[] nouveauTableau = new Object[capacite];
        System.arraycopy(tableau, 0, nouveauTableau, 0, taille);
        tableau = nouveauTableau;
    }

    public void addElement(Object element)
    {
        if (taille == capacite) {
            agrandir();
        }
        tableau[taille] = element;
        taille++;
    }

    public Object getElementAt(int index) {
        if (index < 0 || index >= taille) {
            throw new OutOfMemoryError("L'index est en dehors de la capacité");
        }
        return tableau[index];
    }

    @Override
    public String toString() {
        return "Tableau{" +
                "hashCode=" + hashCode() +
                ", taille=" + taille +
                ", capacite=" + capacite +
                '}';
    }
}
