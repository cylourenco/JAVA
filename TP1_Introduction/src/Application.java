


public class Application {
	
	public static class Voiture {
		public void demarrer()
		{
			System.out.println("Je suis une voiture !\nVroum vroum !\n");
		}
	}

	public static class Camion {
		public void demarrer()
		{
			System.out.println("Je suis un camion !\nPOINNNN POINNNN !\n");
		}
	}
	
    public static void main(String[] arg) {
        Voiture v = new Voiture();
        Camion c = new Camion();
        v.demarrer();      
        c.demarrer();
    }
}
