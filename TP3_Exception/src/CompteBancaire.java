package src;

public class CompteBancaire {

    private double solde = 0.0;       // solde du compte
    private double decouvert = -700.; // découvert autorisé

    public CompteBancaire() {
        System.out.println("Compte Bancaire");
    }

    public void retrait(double montant) throws IllegalArgumentException, AutorisationException{
        double nouveau = solde - montant;
        if (montant<0.0) 
            throw new IllegalArgumentException("Mauvais montant");
        else if (nouveau<decouvert) 
            throw new AutorisationException();
        else 
            solde = nouveau;
    }

    public void entree(double montant) {
        solde += montant;
    }

    @Override
    public String toString() {
        return "Solde du compte " + this.solde + "\nDecouvert autorise " + this.decouvert;
    }

    public double getSolde() {
        return this.solde;
    }

    public double getDecouvert() {
        return this.decouvert;
    }

    public void setSolde(double nv) {
        this.solde = nv;
    }

    public void setDecouvert(double nv) {
        this.decouvert = nv;
    }


}