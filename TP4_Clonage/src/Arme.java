package src;

abstract public class Arme {
    private int numSerie;

    public Arme(int numSerie) {
        this.numSerie = numSerie;
    }

    public Arme(Arme autreArme) {
        this.numSerie = autreArme.getNumSerie();
    }

    public int getNumSerie() {
        return this.numSerie;
    }

    public void setNumSerie(int num) {
        this.numSerie = num;
    }

    @Override
    public Object clone()
    {
        
    }
}
