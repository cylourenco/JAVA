package src;

public class Personne implements Affichable {
    private String      nom;
    private int         age;
    private int         numero;
    public static int  nombre = 0;

    public Personne(String nom, int age) 
    {
       
        if (age > 120 || age < 0){ 
            throw new IllegalArgumentException("Age impossible"); 
        }
        else{
            this.nom = nom;
            this.age = age;
            this.numero = nombre;
            ++nombre;
        }
    }

    public Personne(String nom, int age, boolean b) 
    {
        this(nom, age);
        this.numero = nombre;
        ++nombre;
    }

    String getNom()
    {
        return nom;
    }

    int getAge()
    {
        return age;
    }

    int getNumero()
    {
        return numero;
    }

    static int getNombre()
    {
        return nombre;
    }

    public void setNom(String nom) 
    {
        this.nom = nom;
    }

    public void setAge(int age) 
    {
        this.age = age;
    }

    public void setNumero(int numero) 
    {
        this.numero = numero;
    }

    @Override
    public void afficherDetails() 
    {
        System.out.println(getClass().getSimpleName() +" [nom=" + nom + ", age=" + age + ", numero=" + numero + "]");
    }

}
