package src;

public class Trooper2 extends Trooper {

    private static int compteur = 0;

    public int      id;
    public String   nom;
    public int      niv0Sante;
    public Arme     arme;
    //public Ceinture ceinture;
    public Grade    grade;

    public Trooper2(String s, Arme a, Grade g) {
        this.id = this.compteur;
        setNom(s);
        setNiv0Sante();
        this.arme = a;
        this.grade = g;
        this.compteur++;
    }

    @Override
    public String toString() {
        return "Trooper " + id + " [nom = " + nom + ", niv0Sante = " + niv0Sante + ", arme = " + arme.getNumSerie() + 
                ", grade = " + grade + "]";
            
    }



    @Override
    public Object clone() {
        Trooper2 object = null;
        try {
            object = (Trooper2) (super.clone());
            object.id = compteur++;
        } catch(CloneNotSupportedException cnse) {
            cnse.printStackTrace(System.err);
        }

        object.arme.setArme(Arme).
        
        return object;
    }
}
