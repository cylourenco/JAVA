package src;

public class Etudiant extends Personne 
{
    private int specialite;

    public Etudiant(String nom, int age, int specialite)
    {
        super(nom, age);
        this.specialite = specialite;
    }

    public int getSpecialite() {
        return specialite;
    }

    public void setSpecialite(int specialite) {
        this.specialite = specialite;
    }
    
}
