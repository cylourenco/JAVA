


public class Heritage {

	public static class Mere {

		protected String nom;

		public Mere()
		{
		}

		public Mere(String nom)
		{
			this.nom = nom;
		}

		public void m() {
			System.out.println("Je suis maman "+nom);
		}
	}

	public static class Fille extends Mere {
		public Fille() {
			System.out.println("construction de fille");
			nom = "Angele";
		}

		public Fille(String nom)
		{
			super(nom);
		}

		@Override
		public void m() 
		{
     		System.out.println("methode de fille de l'override "+ nom);
  		}
	}

	public static void main(String a[]) {
		Fille f = new Fille();
		f.m();

		Fille f1 = new Fille("Marie");
		f1.m();
  	}
}

// compiler : javac -d bin -sourcepath src src/Heritage.java 
// executer : java -cp bin Heritage