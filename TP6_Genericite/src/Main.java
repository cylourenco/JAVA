package src;

public class Main {

    public static void main(String[] args) 
    {
        Tableau monTableau = new Tableau();

        for (int i = 1; i <= 5; i++) {
            monTableau.addElement(i);
        }

        int somme = 0;
        for (int i = 0; i < monTableau.getTaille(); i++) {
            somme += (Integer) monTableau.getElementAt(i);
        }

        System.out.println("Somme des éléments : " + somme);

        // Result result = JUnitCore.runClasses(TableauTest.class);

        // for (Failure failure : result.getFailures()) {
        //     System.out.println(failure.toString());
        // }

        // if (result.wasSuccessful()) {
        //     System.out.println("Tous les tests ont réussi.");
        // } else {
        //     System.out.println("Des tests ont échoué.");
        // }
    }

}


